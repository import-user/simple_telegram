simple_telegram

  -m -> msg
    ska skicka medelande
    behöver inte att set person utan om person is not set så ska den ta första
    i config, error if config == null

  -p -> person
    pick person from config
    -> man ska kunna ta också set new person

  -f -> file
    pick file to send to person -> chat
    behöver inte att set person utan om person is not set så ska den ta första
    i config, error if config == null

  -s -> setup
    get the chat_id
    need botTOKEN, error if == null
    *polling*

---

config

[name]
BOT_TOKEN for the bot
chat_id for chat_id to send to


---

Vill du ha mera adv stuff så finns det bliblotek som tar och gör saker bättre
simple_telegram är när man bara vill ta och enkelt ta och skicka något eller
skicka en notis när något händer

fördelar med den här är att den behöver inte ta och hålla på med massa polling
utan den själv tar och post/get när det skickas något


Vill du bara skicka en fil till dig när en chrontab är klar så tar du bara
och sätter inte vilken bot och chat_id som den skickar med och du kan ha så att
du kommer åt config.

t.ex.

$ python3 st.py -f file_name.file

och då kommer den skicka det till dig och du kommer kunna nå den på allt du har
telegram på

---

todo

lägg så att den automatiskt tar och import chat_id och man behöver bara TOKEN

t.ex.

$ python3 st.py -s TOKEN

to get the chat_id you need just to send a text to the bot and st.py will get
all the info it need to setup

-> got info from 'name' the chat_id is 'chat_id'

save to config?

print-> det som den får från updates och lägger in det i config

---
