#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# ! python3
import requests
import configparser
import argparse
import textwrap
import sys
import signal

# tl;dr Telegrambot
# just a simple telegram librery for sending files and text
# You just need requests


def signal_handler(signal, frame):
    # from http://stackoverflow.com/a/4205386
    print('\nQuitting')
    sys.exit(0)


def yes_no(question, default="yes"):
    # from http://stackoverflow.com/a/3041990 mod for python3
    """Ask a yes/no question via input() and return their answer.

    "question" is a string that is presented to the user.
    "default" is the presumed answer if the user just hits <Enter>.
        It must be "yes" (the default), "no" or None (meaning
        an answer is required of the user).

    The "answer" return value is True for "yes" or False for "no".
    """
    valid = {"yes": True, "y": True, "ye": True,
             "no": False, "n": False}
    if default is None:
        prompt = " [y/n] "
    elif default == "yes":
        prompt = " [Y/n] "
    elif default == "no":
        prompt = " [y/N] "
    else:
        raise ValueError("invalid default answer: '%s'" % default)

    while True:
        sys.stdout.write(question + prompt)
        choice = input().lower()
        if default is not None and choice == '':
            return valid[default]
        elif choice in valid:
            return valid[choice]
        else:
            sys.stdout.write("Please respond with 'yes' or 'no' "
                             "(or 'y' or 'n').\n")

# print(config[c[0]]['TOKEN'])


def c():
    "function for getting the config"
    config = configparser.ConfigParser()
    config.read('config.ini')
    c = config.sections()
    # print("c:", c)
    # getting the first in the config
    return config[c[0]]


def a():
    "args for CLI"
    parser = argparse.ArgumentParser(
        description='CLI for simple_telegram',
        formatter_class=argparse.RawDescriptionHelpFormatter,
        epilog=textwrap.dedent('''

    The Config is located in config.ini
        exmple config:

        [BOB]
        TOKEN = 1233456789:h3bdpodjd
        chat_id = 1839457'''))

    parser.add_argument("-m", "--message",
                        help="message to send", type=str)

    parser.add_argument("-p", "--person", help="set person", type=str)
    parser.add_argument("-f", "--file", help="set file to send", type=str)
    parser.add_argument("-s", "--setup", help="token to setup config",
                        type=str, metavar="TOKEN")

    parser.add_argument("-u", "--update", help="token to get updates",
                        nargs='?', default=1, metavar="TOKEN")

    return parser.parse_args()


def sendDocument(file_name, config):

    url = 'https://api.telegram.org/bot%s/sendDocument' % config['TOKEN']

    payload = {
        'chat_id': config['chat_id']
    }

    files = {
        'document': open(file_name, 'rb')
    }

    return requests.post(url, files=files, data=payload)


def sendMessage(text, config):

    url = 'https://api.telegram.org/bot%s/sendMessage' % config['TOKEN']

    payload = {
        'chat_id': config['chat_id'],
        'text': text
    }

    return requests.get(url, params=payload)


def pipe():
    if not sys.stdin.isatty():
        stdin = sys.stdin.readlines()
        return ''.join(stdin)
        sys.stdin = open('/dev/tty')


def setup(config):
    ""
    url = 'https://api.telegram.org/bot%s/getMe' % config
    # print("url", url)

    r = requests.get(url)

    data = r.json()

    print("botName", data["result"]["first_name"])
    # get update for TOKEN
    # pull update
    # parse
    # get config
    # print to config


def getUpdate(config):
    ""
    url = 'https://api.telegram.org/bot%s/getUpdates' % config

    data = {
        'timeout': 20
    }

    r = requests.get(url, params=data)

    data = r.json()
    print("data", data)
    name = data["result"][0]["message"]["from"]["username"]
    msg = data["result"][0]["message"]["text"]

    print("msg from ", name, ":", msg)

    if yes_no("got config for: '" + name + "'\nadd to config?"):
        print("adding", name, "to config")


def main():

    pipeMsg = pipe()
    config = c()
    args = a()

    if pipeMsg:
        sendMessage(str(pipeMsg), config)
        # print("True", pipeMsg)

    if args.message:
        sendMessage(str(args.message), config)

    if args.file:
        sendDocument(str(args.file), config)

    if args.setup:
        setup(str(args.setup))

    if args.update == 1:
        i = input("TOKEN? ")
        if i:
            getUpdate(str(i))

if __name__ == "__main__":
    signal.signal(signal.SIGINT, signal_handler)
    # print("Ctrl+C = Abort")
    main()

# sendDocument('todo.md', config)
# sendMessage('WATMAN!!', config)
