tl;dr

How to get it working:

1. you need python3
2. pip3 install requests

3. setup config.ini
note: I am working to make the setup quick and easy

example config.ini:

>[BOB]  
TOKEN = 1233456789:h3bdpodjd  
chat_id = 1839457


4. $ python3 st.py -m Hello World

FAQ:  
Q: How do I get the TOKEN?  
A: you get it from the BOTFATHER  

Q: how do I get the right chat_id?  
A: http://stackoverflow.com/a/32572159  
