import argparse
import textwrap

parser = argparse.ArgumentParser(
    description='CLI for simple_telegram',
    formatter_class=argparse.RawDescriptionHelpFormatter,
    epilog=textwrap.dedent('''

The Config is located in config.ini
    exmple config:

    [BOB]
    TOKEN = 1233456789:h3bdpodjd
    chat_id = 1839457'''))

parser.add_argument("-m", "--message",
                    help="message to send", type=str)

parser.add_argument("-p", "--person", help="set person", type=str)
parser.add_argument("-f", "--file", help="set file to send", type=str)
parser.add_argument("-s", "--setup", help="token to setup config",
                    type=str, metavar="TOKEN")

args = parser.parse_args()

if args.person:
    print("This is a person", args.person, "the msg:", args.message)
else:
    print("No person, trying to get person from config")
    print("the msg:", args.message)
